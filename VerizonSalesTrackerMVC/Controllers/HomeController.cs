﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Data;
using VerizonSalesTrackerMVC.Entities;

namespace VerizonSalesTrackerMVC.Controllers
{
    public class HomeController : Controller
    {
        string usersSQL = "SELECT EM.iVocalcomID AS AgentID,EM.sFirstName as AgentFirst,EM.sLastName as AgentLast,SUP.iVocalcomID AS SupID,SUP.sFirstName AS SupFirst,SUP.sLastName as SupLast,MNGR.iVocalcomID AS ManID,MNGR.sFirstName as ManFirst,MNGR.sLastName AS ManLast FROM TimeControl.dbo.emEmployee EM JOIN TimeControl.dbo.msClientProgram CP ON EM.iMsClientProgramID= CP.iMsClientProgramID AND CP.sCode LIKE 'VERIZON%' JOIN TimeControl.dbo.msDepartment D ON EM.iMsDepartmentId= D.iMsDepartmentId AND D.sDescription= 'TSR' LEFT JOIN TimeControl.dbo.emEmployee SUP ON EM.iEmSupervisorId= SUP.iEmEmployeeId LEFT JOIN TimeControl.dbo.emEmployee MNGR ON SUP.iEmSupervisorId= MNGR.iEmEmployeeId WHERE EM.dTerminationDate IS NULL";
        string conStr = System.Configuration.ConfigurationManager.ConnectionStrings["OnePlace"].ConnectionString;


        public ActionResult Login()
        {
            if (HttpContext.Session["Users"] == null)
            {
                return RedirectToAction("Index");
            }
            @ViewBag.Message = "";
            return View();
        }

        public ActionResult Refresh()
        {
            HttpContext.Session["SalesTrackerResults"] = null;
            return RedirectToAction("Index");
        }

        public ActionResult Summary()
        {
            if (HttpContext.Session["UserID"] == null)
            {
                return RedirectToAction("Login");
            }
            else
            {
                @ViewBag.UserType = (string)HttpContext.Session["UserType"];
                List<SalesTrackerData>  results = (List<SalesTrackerData>)HttpContext.Session["FilteredTrackerResults"];

                var StatusList = results.Select(s => s.StatusId).Distinct().ToList();

                List<SalesSummaryData> summaryData = new List<SalesSummaryData>();

                foreach (var l in StatusList)
                {
                    SalesSummaryData row = new SalesSummaryData();


                    if (l == 0)
                    {
                        row.Status = "Not Selected";
                    }
                    else
                    {
                        // get it from table                    
                        string sql = "SELECT sStatus FROM [msStatus] WHERE iMsStatusId = " + l.ToString();


                        using (SqlConnection con = new SqlConnection(conStr))
                        {
                            con.Open();
                            using (SqlCommand cmd = new SqlCommand(sql, con))
                            {
                                cmd.CommandType = CommandType.Text;
                                var statusText = cmd.ExecuteScalar();
                                row.Status = (string)statusText;
                            }



                        }
                    }

                    row.GrossAccessoryRevenue = results.Where(w => w.StatusId == l).Sum(s => decimal.Parse(s.GrossAccessoryRevenue)).ToString();
                    row.GrossNewLines = results.Where(w => w.StatusId == l).Sum(s => decimal.Parse(s.GrossNewLines)).ToString();
                    row.GrossUpgrades = results.Where(w => w.StatusId == l).Sum(s => decimal.Parse(s.GrossUpgrades)).ToString();
                    summaryData.Add(row);
                }

                @ViewBag.TotalGrossAccessoryRevenue = summaryData.Sum(s => decimal.Parse(s.GrossAccessoryRevenue));
                @ViewBag.TotalGrossNewLines = summaryData.Sum(s => decimal.Parse(s.GrossNewLines));
                @ViewBag.TotalGrossUpgrades = summaryData.Sum(s => decimal.Parse(s.GrossUpgrades));

                return View(summaryData);
            }
        }


        public ActionResult ManagerSummary()
        {
            if (HttpContext.Session["UserID"] == null)
            {
                return RedirectToAction("Login");
            }
            else
            {
                List<SalesTrackerData> results = (List<SalesTrackerData>)HttpContext.Session["FilteredTrackerResults"];
                int manID = (int)HttpContext.Session["UserID"];
                @ViewBag.UserType = (string)HttpContext.Session["UserType"];

                List<SalesSummarySupervisorData> model = new List<SalesSummarySupervisorData>();
                List<User> users = (List<User>)HttpContext.Session["Users"];
                var supervisorList = users.Where(w => w.ManID == manID).Select(s => new { SupID = s.SupID, SupFirst = s.SupFirst, SupLast = s.SupLast }).Distinct().ToList();

                foreach (var supervisor in supervisorList)
                {
                    var agentList = users.Where(w => w.ManID == supervisor.SupID || w.SupID == supervisor.SupID).Select(s => s.AgentID).Distinct().ToList();

                    SalesSummarySupervisorData row = new SalesSummarySupervisorData();
                    row.SupervisorFirstName = supervisor.SupFirst;
                    row.SupervisorID = supervisor.SupID;
                    row.SupervisorLastName = supervisor.SupLast;
                    row.GrossAccessoryRevenue = results.Where(w => agentList.Contains(int.Parse(w.sAgentId))).Sum(s => decimal.Parse(s.GrossAccessoryRevenue)).ToString();
                    row.GrossNewLines = results.Where(w => agentList.Contains(int.Parse(w.sAgentId))).Sum(s => decimal.Parse(s.GrossNewLines)).ToString();
                    row.GrossUpgrades = results.Where(w => agentList.Contains(int.Parse(w.sAgentId))).Sum(s => decimal.Parse(s.GrossUpgrades)).ToString();
                    model.Add(row);
                }

                @ViewBag.TotalGrossAccessoryRevenue = model.Sum(s => decimal.Parse(s.GrossAccessoryRevenue));
                @ViewBag.TotalGrossNewLines = model.Sum(s => decimal.Parse(s.GrossNewLines));
                @ViewBag.TotalGrossUpgrades = model.Sum(s => decimal.Parse(s.GrossUpgrades));

                return View(model);
            }
        }


        public ActionResult UpdateStatus(long ContactID, int StatusID)
        {
            List<SalesTrackerData> res = (List<SalesTrackerData>)HttpContext.Session["SalesTrackerResults"];
            var orderRecord = res.Where(w => w.iCoContactId == ContactID).FirstOrDefault();
            orderRecord.StatusId = StatusID;
            HttpContext.Session["SalesTrackerResults"] = res;

            // commit update to DB
            /////////////////////////////////////////
            if (StatusID != 0)
            {
                string sql = "INSERT INTO coContactStatusHistory (iCoContactId,iMsStatusId,sCreatedBy) VALUES (" + ContactID.ToString() + "," + StatusID.ToString() + ",'" + ((int)HttpContext.Session["UserID"]).ToString() + "')";
                string sql2 = "UPDATE coContact SET iMsStatusID = " + StatusID.ToString() + " WHERE iCoContactId = " + ContactID.ToString();

                using (SqlConnection con = new SqlConnection(conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, con))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();
                    }

                    using (SqlCommand cmd = new SqlCommand(sql2, con))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();
                    }

                }
            }
            else
            {

                string sql2 = "UPDATE coContact SET iMsStatusID = NULL WHERE iCoContactId = " + ContactID.ToString();

                using (SqlConnection con = new SqlConnection(conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(sql2, con))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();
                    }

                }
            }


                    /////////////////////////////////////////


                    return RedirectToAction("Index");
        }

        public ActionResult Search(string searchFilter)
        {
            HttpContext.Session["Search"] = searchFilter;
            HttpContext.Session["Page"] = 1;
            return RedirectToAction("Index");
        }

        public ActionResult ChangePage(int Page)
        {
            HttpContext.Session["Page"] = Page;
            return RedirectToAction("Index");
        }

        public ActionResult Sort(string sortFilter)
        {

            if ((string)HttpContext.Session["Sort"] == sortFilter)
            {
                if ((string)HttpContext.Session["SortDirection"] == "ASC")
                {
                    HttpContext.Session["SortDirection"] = "DESC";
                }
                else
                {
                    HttpContext.Session["SortDirection"] = "ASC";
                }
            }
            else
            {
                HttpContext.Session["SortDirection"] = "ASC";
                HttpContext.Session["Sort"] = sortFilter;

            }
            
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            List<int> filterAgents = new List<int>();
            List<User> users = (List<User>)HttpContext.Session["Users"];
          
            bool isValid = false;
            if (model.FirstName == string.Empty || model.LastName == string.Empty || model.AgentID == 0)
            {
                @ViewBag.Message = "Please provide all values.";
            }
            else
            {
                // Check if Manager
                var manager = users.Where(w => ((w.ManFirst == null) ? "" : w.ManFirst.ToUpper()) == model.FirstName.ToUpper() && ((w.ManLast == null) ? "" : w.ManLast.ToUpper()) == model.LastName.ToUpper() && w.ManID == model.AgentID).FirstOrDefault();
                if (manager != null)
                {
                    HttpContext.Session["UserType"] = "Manager";
                    HttpContext.Session["UserID"] = manager.ManID;
                    isValid = true;
                    // get manager's agents
                    var supList = users.Where(w => w.ManID == manager.ManID).Select(s => s.SupID).Distinct().ToList();
                    var agentIDList = users.Where(w => w.ManID == manager.ManID || w.SupID == manager.ManID || supList.Contains(w.ManID)).Select(s => s.AgentID).Distinct().ToList();
                    filterAgents = agentIDList;
                }
                else
                {
                    var supervisor = users.Where(w => ((w.SupFirst == null) ? "" : w.SupFirst.ToUpper()) == model.FirstName.ToUpper() && ((w.SupLast == null) ? "" : w.SupLast.ToUpper()) == model.LastName.ToUpper() && w.SupID == model.AgentID).FirstOrDefault();
                    if (supervisor != null)
                    {
                        HttpContext.Session["UserType"] = "Supervisor";
                        HttpContext.Session["UserID"] = supervisor.SupID;
                        isValid = true;
                        // get supervisors's managers
                        var agentIDList = users.Where(w => w.SupID == supervisor.SupID).Select(s => s.AgentID).Distinct().ToList();
                        filterAgents = agentIDList;
                    }
                    else
                    {
                        // Check if agent
                        var agent = users.Where(w => w.AgentFirst.ToUpper() == model.FirstName.ToUpper() && w.AgentLast.ToUpper() == model.LastName.ToUpper() && w.AgentID == model.AgentID).FirstOrDefault();
                        if (agent != null)
                        {
                            HttpContext.Session["UserType"] = "Agent";
                            HttpContext.Session["UserID"] = agent.AgentID;
                            isValid = true;
                            filterAgents.Add(agent.AgentID);
                        }
                        else
                        {
                            @ViewBag.Message = "Invalid Login";
                        }
                    }
                }

            }

            if (isValid)
            {
                HttpContext.Session["filterAgents"] = filterAgents;
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
            
        }

        public ActionResult Index()
        {

            // Set Defaults
            if (HttpContext.Session["Search"] == null)
            {
                HttpContext.Session["Search"] = "";
            }

            if (HttpContext.Session["Sort"] == null)
            {
                HttpContext.Session["Sort"] = "dCallStart";
            }

            if (HttpContext.Session["SortDirection"] == null)
            {
                HttpContext.Session["SortDirection"] = "ASC";
            }

            if (HttpContext.Session["Page"] == null)
            {
                HttpContext.Session["Page"] = 1;
            }


            // Let's get the User Data if we don't have it
            if (HttpContext.Session["Users"] == null)
            {
                List<User> users = new List<User>();
                using (SqlConnection con = new SqlConnection(conStr))
                {
                    using (SqlCommand cmd = new SqlCommand(usersSQL, con))
                    {
                        cmd.CommandType = CommandType.Text;
                        con.Open();

                        var reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            User user = new User();
                            user.AgentFirst = (string)reader["AgentFirst"];
                            user.AgentID = (int)reader["AgentID"];
                            user.AgentLast = (string)reader["AgentLast"];
                            if (reader["ManFirst"] != DBNull.Value)
                            {
                               user.ManFirst= (string)reader["ManFirst"]; 
                            }
                            
                            if (reader["ManID"] != DBNull.Value)
                            {
                                user.ManID = (int)reader["ManID"];
                            }

                            if (reader["ManLast"] != DBNull.Value)
                            {
                              user.ManLast= (string)reader["ManLast"]; 
                            }

                            if (reader["SupFirst"] != DBNull.Value)
                            {
                              user.SupFirst = (string)reader["SupFirst"];  
                            }

                            if (reader["SupID"] != DBNull.Value)
                            {
                               user.SupID = (int)reader["SupID"];
                            }

                            if (reader["SupLast"] != DBNull.Value)
                            {
                               user.SupLast = (string)reader["SupLast"];
                            }
                            

                            users.Add(user);
                        }
                    }

                }

                HttpContext.Session["Users"] = users;
            }


            DataTable dt = new DataTable();
            List<SalesTrackerData> res = new List<SalesTrackerData>();
            if (HttpContext.Session["SalesTrackerResults"] == null)
            {
                // Get it
                using (SqlConnection con = new SqlConnection(conStr))
                {
                    using (SqlCommand cmd = new SqlCommand("vzwaudit_sp_TrackerSearch", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@agentId", DBNull.Value));
                        con.Open();
                        var reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            SalesTrackerData row = new SalesTrackerData();
                            row.dCallStart = (DateTime)reader["dCallStart"];

                            row.sAgentId = (string)reader["sAgentId"];
                            row.iCoContactId = (long)reader["iCoContactId"];

                            if (reader["InStorePickUp"] == DBNull.Value)
                            {
                                row.InStorePickUp = "No";
                            }
                            else
                            {
                                row.InStorePickUp = (string)reader["InStorePickUp"];
                            }
                            
                            row.iOTDLeadId = (long)reader["iOTDLeadId"];

                            if (reader["LocationCode"] == DBNull.Value)
                            {
                                row.LocationCode = string.Empty;
                            }
                            else
                            {
                                row.LocationCode = (string)reader["LocationCode"];
                            }

                            
                            
                            row.sAgentName = (string)reader["sAgentName"];

                            if (reader["TMP"] == DBNull.Value)
                            {
                                row.TMP = string.Empty;
                            }
                            else
                            {
                                row.TMP = (string)reader["TMP"];
                            }

                            if (reader["GrossAccessoryRevenue"] == DBNull.Value)
                            {
                                row.GrossAccessoryRevenue = "0";
                            }
                            else
                            {
                                row.GrossAccessoryRevenue = (string)reader["GrossAccessoryRevenue"];
                            }

                            if (reader["GrossNewLines"] == DBNull.Value)
                            {
                                row.GrossNewLines = "0";
                            }
                            else
                            {
                                row.GrossNewLines = (string)reader["GrossNewLines"];
                            }

                            if (reader["GrossUpgrades"] == DBNull.Value)
                            {
                                row.GrossUpgrades = "0";
                            }
                            else
                            {
                                row.GrossUpgrades = (string)reader["GrossUpgrades"];
                            }

                            if (reader["OrderNumber"] == DBNull.Value)
                            {
                                row.OrderNumber = string.Empty;
                            }
                            else
                            {
                                row.OrderNumber = (string)reader["OrderNumber"];
                            }

                            if (reader["StatusId"] == DBNull.Value)
                            {
                                row.StatusId = 0;
                            }
                            else
                            {
                                row.StatusId = (int)reader["StatusId"];
                            }


                            res.Add(row);
                        }
                    }
                }
                HttpContext.Session["SalesTrackerResults"] = res;
            }
            else
            {
                res = (List<SalesTrackerData>)HttpContext.Session["SalesTrackerResults"];
            }

            
            // if  not logged in redirect to login
            if (HttpContext.Session["UserID"] == null)
            {
                return RedirectToAction("Login");
            }
            else
            {

                List<int> filterAgents = (List<int>)HttpContext.Session["filterAgents"];


                    res = res.Where(w => filterAgents.Contains(int.Parse(w.sAgentId))).ToList();
                    if ((string)HttpContext.Session["UserType"] == "Agent")
                    {
                        res = res.Where(w => w.StatusId != 199).ToList();
                    }

                    HttpContext.Session["FilteredTrackerResults"] = res;

                    if ((string)HttpContext.Session["Search"] != string.Empty)
                    {
                         res = res.Where(w => w.dCallStart.ToString("MM/dd/yy") == (string)HttpContext.Session["Search"]  || w.iOTDLeadId.ToString().Contains((string)HttpContext.Session["Search"]) || w.LocationCode.ToUpper().Contains(((string)HttpContext.Session["Search"]).ToUpper()) || w.OrderNumber.Contains((string)HttpContext.Session["Search"]) || w.sAgentId.Contains((string)HttpContext.Session["Search"]) || w.sAgentName.ToUpper().Contains(((string)HttpContext.Session["Search"]).ToUpper())).ToList();
                    }
                   

                    if ((string)HttpContext.Session["Sort"] == "dCallStart")
                    {
                        if ((string)HttpContext.Session["SortDirection"] == "ASC")
                        {
                            res = res.OrderBy(o => o.dCallStart).ToList();
                        }
                        else
                        {
                            res = res.OrderByDescending(o => o.dCallStart).ToList();
                        }
                    }

                    if ((string)HttpContext.Session["Sort"] == "iOTDLeadId")
                    {
                        if ((string)HttpContext.Session["SortDirection"] == "ASC")
                        {
                            res = res.OrderBy(o => o.iOTDLeadId).ToList();
                        }
                        else
                        {
                            res = res.OrderByDescending(o => o.iOTDLeadId).ToList();
                        }
                    }

                    if ((string)HttpContext.Session["Sort"] == "iCoContactId")
                    {
                        if ((string)HttpContext.Session["SortDirection"] == "ASC")
                        {
                            res = res.OrderBy(o => o.iCoContactId).ToList();
                        }
                        else
                        {
                            res = res.OrderByDescending(o => o.iCoContactId).ToList();
                        }
                    }

                    if ((string)HttpContext.Session["Sort"] == "sAgentId")
                    {
                        if ((string)HttpContext.Session["SortDirection"] == "ASC")
                        {
                            res = res.OrderBy(o => o.sAgentId).ToList();
                        }
                        else
                        {
                            res = res.OrderByDescending(o => o.sAgentId).ToList();
                        }
                    }

                    if ((string)HttpContext.Session["Sort"] == "sAgentName")
                    {
                        if ((string)HttpContext.Session["SortDirection"] == "ASC")
                        {
                            res = res.OrderBy(o => o.sAgentName).ToList();
                        }
                        else
                        {
                            res = res.OrderByDescending(o => o.sAgentName).ToList();
                        }
                    }

                    if ((string)HttpContext.Session["Sort"] == "OrderNumber")
                    {
                        if ((string)HttpContext.Session["SortDirection"] == "ASC")
                        {
                            res = res.OrderBy(o => o.OrderNumber).ToList();
                        }
                        else
                        {
                            res = res.OrderByDescending(o => o.OrderNumber).ToList();
                        }
                    }

                    if ((string)HttpContext.Session["Sort"] == "LocationCode")
                    {
                        if ((string)HttpContext.Session["SortDirection"] == "ASC")
                        {
                            res = res.OrderBy(o => o.LocationCode).ToList();
                        }
                        else
                        {
                            res = res.OrderByDescending(o => o.LocationCode).ToList();
                        }
                    }

                    if ((string)HttpContext.Session["Sort"] == "GrossNewLines")
                    {
                        if ((string)HttpContext.Session["SortDirection"] == "ASC")
                        {
                            res = res.OrderBy(o => o.GrossNewLines).ToList();
                        }
                        else
                        {
                            res = res.OrderByDescending(o => o.GrossNewLines).ToList();
                        }
                    }

                    if ((string)HttpContext.Session["Sort"] == "GrossAccessoryRevenue")
                    {
                        if ((string)HttpContext.Session["SortDirection"] == "ASC")
                        {
                            res = res.OrderBy(o => o.GrossAccessoryRevenue).ToList();
                        }
                        else
                        {
                            res = res.OrderByDescending(o => o.GrossAccessoryRevenue).ToList();
                        }
                    }

                    if ((string)HttpContext.Session["Sort"] == "GrossUpgrades")
                    {
                        if ((string)HttpContext.Session["SortDirection"] == "ASC")
                        {
                            res = res.OrderBy(o => o.GrossUpgrades).ToList();
                        }
                        else
                        {
                            res = res.OrderByDescending(o => o.GrossUpgrades).ToList();
                        }
                    }

                    if ((string)HttpContext.Session["Sort"] == "TMP")
                    {
                        if ((string)HttpContext.Session["SortDirection"] == "ASC")
                        {
                            res = res.OrderBy(o => o.TMP).ToList();
                        }
                        else
                        {
                            res = res.OrderByDescending(o => o.TMP).ToList();
                        }
                    }

                    if ((string)HttpContext.Session["Sort"] == "InStorePickUp")
                    {
                        if ((string)HttpContext.Session["SortDirection"] == "ASC")
                        {
                            res = res.OrderBy(o => o.InStorePickUp).ToList();
                        }
                        else
                        {
                            res = res.OrderByDescending(o => o.InStorePickUp).ToList();
                        }
                    }


                    @ViewBag.Page = (int)HttpContext.Session["Page"];
                    @ViewBag.Count = res.Count();

                    int totalPages = 1;
                    if (res.Count() > 20)
                    {
                        totalPages = (res.Count() / 20);
                        if ((res.Count() % 20) != 0)
                        {
                            totalPages++;
                        }
                    }

                    @ViewBag.PageCount = totalPages;

                    int skip = (((int)HttpContext.Session["Page"] - 1) * 20);
                    res = res.Skip(skip).Take(20).ToList();

                    @ViewBag.UserType = (string)HttpContext.Session["UserType"];
                    return View(res);



            }


        }


    }
}