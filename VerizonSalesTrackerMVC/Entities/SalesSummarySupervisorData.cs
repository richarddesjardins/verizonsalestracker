﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VerizonSalesTrackerMVC.Entities
{
    [Serializable]
    public class SalesSummarySupervisorData
    {

        public int SupervisorID { get; set; }
        public string SupervisorFirstName { get; set; }
        public string SupervisorLastName { get; set; }
        public string GrossNewLines { get; set; }
        public string GrossAccessoryRevenue { get; set; }
        public string GrossUpgrades { get; set; }

    }
}