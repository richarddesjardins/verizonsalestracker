﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VerizonSalesTrackerMVC.Entities
{
    [Serializable]
    public class SalesTrackerData
    {
        //iOTDLeadId	iCoContactId	sAgentId	sAgentName	dCallStart	OrderNumber	LocationCode	GrossNewLines	GrossAccessoryRevenue	GrossUpgrades	TMP	InStorePickUp
        public long iOTDLeadId { get; set; }
        public long iCoContactId { get; set; }
        public string sAgentId { get; set; }
        public string sAgentName { get; set; }
        public DateTime dCallStart { get; set; }
        public string OrderNumber { get; set; }
        public string LocationCode { get; set; }
        public string GrossNewLines { get; set; }
        public string GrossAccessoryRevenue { get; set; }
        public string GrossUpgrades { get; set; }
        public string TMP { get; set; }
        public string InStorePickUp { get; set; }
        public int StatusId { get; set; }
    }
}