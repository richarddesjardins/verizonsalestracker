﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VerizonSalesTrackerMVC.Entities
{
    public class LoginModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int AgentID { get; set; }
    }
}