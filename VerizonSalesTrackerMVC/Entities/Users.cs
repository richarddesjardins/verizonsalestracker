﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VerizonSalesTrackerMVC.Entities
{
    [Serializable]
    public class User
    {
        public int AgentID { get; set; }
        public string AgentFirst { get; set; }
        public string AgentLast { get; set; }
        public int SupID { get; set; }
        public string SupFirst { get; set; }
        public string SupLast { get; set; }
        public int ManID { get; set; }
        public string ManFirst { get; set; }
        public string ManLast { get; set; }
    }
}